import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import axios from 'axios';

import KartoList from '../components/KartoList';

export default class KartoPage extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			kartos: [],
			loading: false,
			error: false,
		};
	}

	static navigationOptions = {
		title: 'Kartos',
	};

	componentDidMount() {
		this.setState({ loading: true });
		axios
			.get('https://my-json-server.typicode.com/GustavoSoaresss/api/anuncios')
			.then(response => {
				console.log(response);
				this.setState({
					kartos: response.data,
					loading: false,
				});
			}).catch(error => {
				console.log(error);
				this.setState({
					loading: false,
					error: true,
				})
			});
	}

	render() {
		return (
			<View style={styles.container}>
				{
					this.state.loading
						? <ActivityIndicator size="large" color="#6ca2f7" />
						: this.state.error
							? <Text style={styles.error}>Ops... Algo deu errado =(</Text>
							: <KartoList
								kartos={this.state.kartos}
								onPressItem={pageParams => {
									this.props.navigation.navigate('KartoDetail', pageParams);
								}}
							/>
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
	},
	error: {
		color: 'red',
		alignSelf: 'center',
		fontSize: 18,
	}
});