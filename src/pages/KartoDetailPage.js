import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ScrollView } from 'react-native';

import Line from '../components/Line';

export default class KartoDetailPage extends Component {
  static navigationOptions = ({ navigation }) => {
    const kartoName = navigation.state.params.karto.title;
    return {
      title: kartoName
    };
  };

  render() {
    const { karto } = this.props.navigation.state.params;

    return (
      <ScrollView style={styles.container}>
        <Image source={{ uri: karto.picture }} style={styles.avatar} />
        <View style={styles.detailContainer}>
          <Text style={styles.description}>{karto.description}</Text>
          <Line label="Preço:" content={karto.price} />
          <Line label="Quantidade:" content={karto.amount} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15
  },
  avatar: {
    aspectRatio: 1
  },
  detailContainer: {
    backgroundColor: '#e2f9ff',
    marginTop: 20,
    marginBottom: 20,
    elevation: 1
  },
  description: {
    fontWeight: 'bold',
    fontSize: 18,
    padding: 5,
    borderWidth: 1,
    borderColor: '#C5C5C5'
  }
});
