import React from 'react';
import { FlatList, StyleSheet } from 'react-native';

import KartoListItem from './KartoListItem';

const KartoList = props => {
  const { kartos, onPressItem } = props;

  return (
    <FlatList
      style={styles.container}
      data={kartos}
      renderItem={({ item }) => (
        <KartoListItem karto={item} navigateToKartoDetail={onPressItem} />
      )}
      keyExtractor={item => item.id.toString()}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e2f9ff'
  }
});

export default KartoList;
