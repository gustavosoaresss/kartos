import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';

const KartoListItem = props => {
  const { karto, navigateToKartoDetail } = props;
  return (
    <TouchableOpacity
      onPress={() => {
        navigateToKartoDetail({ karto });
      }}
    >
      <View style={styles.line}>
        <Image style={styles.avatar} source={{ uri: karto.picture }} />
        <Text style={styles.lineTitle}>{`${karto.title}`}</Text>
        <Text style={styles.lineTitle}>{`Preço: ${karto.price}`}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  line: {
    height: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#bbb',
    alignItems: 'center',
    flexDirection: 'row'
  },
  lineTitle: {
    fontSize: 20,
    paddingLeft: 15,
    flex: 4
  },
  linePrice: {
    fontSize: 20,
    paddingLeft: 15,
    flex: 4
  },
  avatar: {
    aspectRatio: 1,
    flex: 1,
    marginLeft: 15,
    borderRadius: 50
  }
});

export default KartoListItem;
