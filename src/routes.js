import { createStackNavigator, createAppContainer } from 'react-navigation';
import KartoPage from './pages/KartoPage';
import KartoDetailPage from './pages/KartoDetailPage';

const AppNavigator = createStackNavigator(
  {
    Main: {
      screen: KartoPage
    },
    KartoDetail: {
      screen: KartoDetailPage
    }
  },
  {
    defaultNavigationOptions: {
      headerTintColor: '#000',
      headerStyle: {
        backgroundColor: '#6ca2f7',
        borderBottomWidth: 1,
        borderBottomColor: '#C5C5C5'
      },
      headerTitleStyle: {
        color: '#000',
        fontSize: 30
      }
    }
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
